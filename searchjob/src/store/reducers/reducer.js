const initialState = {
    searchedKeyword: "",
};

const reducer = (state = initialState ,action) => {
    switch(action.type){
        case 'INPUT_CHANGE_HANDLER':
            return{
                ...state,
                searchedKeyword: state.searchedKeyword.concat(action.value)
            };
    }

    return state;
}

export default reducer;