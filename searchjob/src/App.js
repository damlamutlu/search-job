import './App.css';
import SearchJob from './components/SearchJob';

function App() {
  return (
    <div >
      <SearchJob />
    </div>
  );
}

export default App;
