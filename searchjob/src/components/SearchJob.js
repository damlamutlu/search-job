import React, { Component } from 'react';
import SearchArea from './search/SearchArea';
import ListArea from './list/ListArea';
import 'primereact/resources/themes/saga-blue/theme.css';
import 'primereact/resources/primereact.min.css';
import 'primeicons/primeicons.css';
import './SearchJob.css';



class SearchJob extends Component{

    constructor(props) {
        super(props);

        this.state = {
            isSearched : false,
            responseJobs: [],
            responseSearchedJobs:[],
            text : ""
        };
        this.getFilterParameter = this.getFilterParameter.bind(this)
      }

    inputChangeHandler(text){
        this.setState({ text: text });
    }

    getFilterParameter(){
        let searchedWord = this.state.text;
        let companyId = null;
        let qualificaitonId = null;
        let workingTimeId = null;
        let employmentTypeId = null;
        this.state.responseJobs.forEach((element) => {
                if(element.attributes.company.name === searchedWord){
                    companyId = element.attributes.company.id;
                }
                element.attributes.qualifications.forEach((qualificationElement) => {
                    if(qualificationElement.title === searchedWord){
                        qualificaitonId = qualificationElement.id;
                    }
                });

                element.attributes.workingTimes.forEach((workingTimeElement) => {
                    if(workingTimeElement.title === searchedWord){
                        workingTimeId = workingTimeElement.id;
                    }
                });

                element.attributes.employmentTypes.forEach((employmentTypeElement) => {
                    if(employmentTypeElement.title === searchedWord){
                        employmentTypeId = employmentTypeElement.id;
                    }
                });
        });

        if(companyId !== null){
            this.fetchSerachedJob("filter.company" , companyId);
        }else if(qualificaitonId !== null){
            this.fetchSerachedJob("filter.qualification" , qualificaitonId);
        }
        else if(workingTimeId !== null){
            this.fetchSerachedJob("filter.working_time" , workingTimeId);
        }else if(employmentTypeId !== null){
            this.fetchSerachedJob("filter.employment_type" , employmentTypeId);
        }else if(searchedWord === "single" || searchedWord === "remote"|| searchedWord === "fieldService"){
            this.fetchSerachedJob("filter.location_type" , searchedWord);
        }else{
            this.setState({responseSearchedJobs : null});
            this.setState({ text: "" });
                this.setState({ isSearched : true });
        }
        
    }

    fetchSerachedJob(filterParameter , requirementParameter){
        fetch('https://api.joblocal.de/v4/search-jobs?' + filterParameter + "=" + requirementParameter)
            .then(res => res.json())
            .then((results) => {
                this.setState({responseSearchedJobs : results.included})
                this.setState({ text: "" })
                this.setState({ isSearched : true })
            })
            .catch(error=>console.log(error));
    }

    
    fetchJobs() {
        fetch('https://api.joblocal.de/v4/search-jobs')
        .then(res => res.json())
        .then((results) => {
            this.setState({responseJobs : results.included})
        })
        .catch(error=>console.log(error));
    }  
    
    componentDidMount(){
        this.fetchJobs(); 
    }

    render(){
        return (
            <div className="main-container">
                <div>
                    <SearchArea 
                    text = { this.state.text }
                    onChange = { (event) => this.inputChangeHandler(event.target.value) } 
                    onClick = { this.getFilterParameter } />   
                </div>
                <div className="list-container">
                    <ListArea jobList={this.state.isSearched === true ? this.state.responseSearchedJobs : this.state.responseJobs } />
                </div>

            </div>
        );
    }
} export default SearchJob;