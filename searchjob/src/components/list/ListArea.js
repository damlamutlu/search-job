import React, { Component } from 'react';
import { Card } from 'primereact/card';

class ListArea extends Component{
    render(){
        return(
            <div>
                    {this.props.jobList === null ? 
                        <Card style = {{ color:"#e39d46" , fontSize:"17px" , textAlign: "center" }}title={ "Job posting was not found!"} /> 
                        : this.props.jobList.map((item) => {
                            return(
                                <Card title= {item.attributes.title} style={{ margin:10 }}>
                                <p style={{ fontSize:"11px" }}> <b> Responsibilities : </b>  {item.attributes.responsibilities}<br></br>
                                <b>Requirement : </b> {item.attributes.requirements}<br></br>
                                <b>Benefits : </b> {item.attributes.benefits}<br></br>
                                <b>Qualifications : </b> {item.attributes.qualifications.map((qualificationsItem) => {
                                    return(
                                        <li>{ qualificationsItem.title }</li>
                                    )
                                })}
                                <b>Work Time : </b> {item.attributes.workingTimes.map((workTimesItem) => {
                                    return(
                                        <li>{ workTimesItem.title }</li>
                                    )
                                })}
                                <b>Employment Type : </b> {item.attributes.employmentTypes.map((employmentTypeItem) => {
                                    return(
                                        <li>{ employmentTypeItem.title }</li>
                                    )
                                })}
                                <b>Company Name : </b> {item.attributes.company.name}<br></br>
                                <b>City : </b> {item.attributes.location.city} </p>
                                </Card>
                            )
                        })}
            </div>
        );
    }
}export default ListArea;