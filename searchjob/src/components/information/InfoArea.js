import React, { Component } from 'react';
import { Button } from 'primereact/button';
import { Dialog } from 'primereact/dialog';

class InfoArea extends Component{
    constructor(props) {
        super(props);

        this.state = {
            isVisible: false,
            position: 'center'
        };

        this.onShow = this.onShow.bind(this);
        this.onHide = this.onHide.bind(this);
    }
    onShow() {
     this.setState({ isVisible: true })
    }

    onHide() {
        this.setState({ isVisible : false });
    }

    render(){
        return(
            <div>
                    <Button style={{ marginTop: "10px" }}  className="p-button-outlined" label="Information" icon="pi pi-external-link" onClick={() => this.onShow()} />
                    <Dialog header="Information" visible={this.state.isVisible} style={{ width: '50vw' }}  onHide={() => this.onHide()}>
                        Search can be used with this parameters :
                            <li>Company Name</li>
                            <li>Working Time</li>
                            <li>Employment Type</li>
                            <li>Location Type :
                                <li style = {{ fontSize:"10px" ,marginLeft: "10px" }}>single</li>
                                <li style = {{ fontSize:"10px" ,marginLeft: "10px"}}>remote</li>
                                <li style = {{ fontSize:"10px" ,marginLeft: "10px"}}>fieldService</li>
                            </li>
                    </Dialog>
            </div>
        );
    }

}export default InfoArea;