import React, { Component } from 'react';
import { Button } from 'primereact/button';
import './SearchArea.css'
import { InputText } from 'primereact/inputtext';
import InfoArea from '../information/InfoArea';



class SearchArea extends Component{

    render() {
        return (
            <div className='search-area'>
                <div className="search-area-item">
                    <InputText value={this.props.text}  style={{width:"400px"}} placeholder="Enter your keyword here.." onChange={this.props.onChange}/>
                </div>
                <div className="search-area-item">
                    <Button label="Search" className="p-button-outlined" onClick={ this.props.onClick }/>
                </div>
                <InfoArea /> 
            </div>
  
        );
      }
}

export default SearchArea;